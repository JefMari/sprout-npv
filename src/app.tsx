import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { useState, Fragment } from 'react';
type Props = {
  investment: number;
  discount: number;
  cashflows: number[];
};

const initialValue: Props = {
  investment: 0,
  discount: 0,
  cashflows: [123, 5454, 123]
};

const App = () => {
  const [npv, setNpv] = useState<Props>(initialValue);
  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log(npv)
    alert('check console for values of npv! ')
  };
  const handleChange = (index: number, value: any) => {
    const cashflows = npv.cashflows;
    value = value.replace(/[^0-9-]+/g, '');

    // The real pattern you are looking for
    var pattern = /([-])?([0-9]+)/g;
    var matches = value.match(pattern);
    if (matches) {
      value = matches[0];
    }
    cashflows[index] = value;
    setNpv({ ...npv, cashflows: cashflows });
  };
  const handleRemove = (index: number) => {
    const cashflows = npv.cashflows;
    cashflows.splice(index, 1);
    setNpv({ ...npv, cashflows: cashflows });
  };
  const handleInsert = () => {
    const cashflows = npv.cashflows;
    cashflows.push(0);
    setNpv({ ...npv, cashflows: cashflows });
  };
  return (
    <div className="flex justify-center items-center bg-gray-200 h-screen w-screen">
      <div className="w-full max-w-lg">
        <form onSubmit={() => handleSubmit(event)} className="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4">
          <h1 className="bg-white text-gray-700 text-4xl">Calculate your NPV</h1>
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="InitialInvestment">
              Initial Investment
            </label>
            <input
              value={npv.investment}
              onChange={(ev: React.ChangeEvent<HTMLInputElement>): void => {
                setNpv({ ...npv, investment: parseInt(ev.target.value) });
              }}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="InitialInvestment"
              type="number"
              placeholder="Initial Investment"
            />
          </div>
          <div className="mb-6">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="DiscountRate">
              Discount Rate
            </label>
            <input
              value={npv.discount}
              onChange={(ev: React.ChangeEvent<HTMLInputElement>): void => {
                setNpv({ ...npv, discount: ev.target.value ? parseInt(ev.target.value) : 0 });
              }}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="DiscountRate"
              type="text"
              min="0"
              placeholder="Discount Rate"
            />
          </div>
          <div className="mb-6">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="Cashflows">
              Cashflows
              <button onClick={() => handleInsert()} className="bg-gray-200 hover:bg-green-600 hover:text-white font-bold shadow-lg rounded px-2 ml-2" type="button">
                + Insert Cashflow
              </button>
            </label>
            {npv.cashflows.map((item, i) => {
              return (
                <Fragment key={`Cashflow_${i + 1}`}>
                  <label className="block text-gray-500 text-xs font-bold mb-2" htmlFor={`Cashflow_${i + 1}`}>
                    {`Cashflow ${i + 1}`}
                  </label>
                  <div className="flex items-center border-teal-500 py-2">
                    <input
                      value={item}
                      onChange={(ev: React.ChangeEvent<HTMLInputElement>): void => {
                        handleChange(i, ev.target.value);
                      }}
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id={`Cashflow_${i + 1}`}
                      type="text"
                      placeholder="Cashflow"
                    />
                    <button
                      onClick={() => {
                        handleRemove(i);
                      }}
                      className="flex-shrink-0 border-transparent border-4 text-teal-500 hover:text-teal-800 text-sm py-1 px-2 rounded"
                      type="button"
                    >
                      Cancel
                    </button>
                  </div>
                </Fragment>
              );
            })}
          </div>
          <div className="flex items-center justify-between">
            <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
              Calculate
            </button>
          </div>
        </form>
        <p className="text-center text-gray-500 text-xs">&copy;2020 Sprout NPV. All rights reserved.</p>
      </div>
    </div>
  );
};

export default hot(App);
