# NPV

Calculate your net present value

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To run in hot module reloading mode:

```sh
npm start
```

## Running

Open `http://localhost:8081/` in your browser